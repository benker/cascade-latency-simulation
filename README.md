# Simulate cascade cloud services latency

the code demostrate the following topology, each layer represent a microservices

```

			┌────────┐
    ┌───────►Layer2-1├────────┐
    │       └────────┘        │
    │                         │
┌───┴──┐    ┌────────┐   ┌────▼───┐
│Layer1├────►Layer2-2├───►Layer3-1│
└───┬──┘    └────────┘   └────▲───┘
    │                         │
    │       ┌────────┐        │
    └───────►Layer2-3├────────┘
            └────────┘
```

by tunning the latency and concurrency in code, may give insight how cascaded cloud services
can be tuned to achieve a lower latency.

