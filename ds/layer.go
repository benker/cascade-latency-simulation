package ds

import (
	"context"
	"log"
	"sync/atomic"
	"time"

	"golang.org/x/sync/semaphore"
)

type Layer struct {
	name				string
	neworktLatencyMs	int
	processingLatencyMs	int
	maxConcurrent		*semaphore.Weighted
	layers				[]*Layer
	totalRequestHandled	int32
	totalUSUsed			int64
}

func NewLayer(name string, networkLatencyMs, processingLatencyMs, maxConcurrent int, layers ...*Layer) (l *Layer) {
	l = &Layer{
		name: name,
		neworktLatencyMs: networkLatencyMs,
		processingLatencyMs: processingLatencyMs,
		maxConcurrent: semaphore.NewWeighted(int64(maxConcurrent)),
		layers: nil,
		totalRequestHandled: 0,
		totalUSUsed: 0,
	}
	for _, layer := range layers {
		l.layers = append(l.layers, layer)
	}
	return
}

func (l *Layer) Execute() {
	start := time.Now()
	err := l.maxConcurrent.Acquire(context.TODO(), 1)
	if err != nil {
		log.Fatalf("fail for semaphore: %v", err)
	}
	time.Sleep(time.Millisecond * time.Duration(l.neworktLatencyMs))
	time.Sleep(time.Millisecond * time.Duration(l.processingLatencyMs))
	for _, layer := range l.layers {
		layer.Execute()
	}
	l.maxConcurrent.Release(1)
	used := time.Since(start).Microseconds()
	atomic.AddInt32(&l.totalRequestHandled, 1)
	atomic.AddInt64(&l.totalUSUsed, used)
}

func (l *Layer) PrintResult() {
	totalTime := time.Microsecond * time.Duration(l.totalUSUsed)
	log.Printf("%s handled requests %d, avg-latency: %v", l.name, l.totalRequestHandled, totalTime/time.Duration(l.totalRequestHandled))
}
