package main

import (
	"log"
	"sync"
	"time"

	"gitlab.com/benker/cascade-latency-simulation/ds"
)


func main() {

	microserviceCon := 48*40
	wg := &sync.WaitGroup{}
	totalRequest := 20000
	networkLatencyMs := 40
	processingLatencyMs := 1

	/* Layer */
	layer3_1 := ds.NewLayer("layer3-1", networkLatencyMs, processingLatencyMs, 60000)

	layer2_1 := ds.NewLayer("layer2-1", networkLatencyMs, processingLatencyMs, microserviceCon, layer3_1)
	layer2_2 := ds.NewLayer("layer2-2", networkLatencyMs, processingLatencyMs, microserviceCon, layer3_1)
	layer2_3 := ds.NewLayer("layer2-3", networkLatencyMs, processingLatencyMs, microserviceCon, layer3_1)

	layer1 := ds.NewLayer("layer1", networkLatencyMs, processingLatencyMs, microserviceCon*10, layer2_1, layer2_2, layer2_3)
	/* End Layer */

	start := time.Now()

	for i:=0; i<totalRequest; i++ {
		wg.Add(1)
		go func() {
			layer1.Execute()
			wg.Done()
		}()
	}
	wg.Wait()
	
	used := time.Since(start)
	layer1.PrintResult()
	layer2_1.PrintResult()
	layer2_2.PrintResult()
	layer2_3.PrintResult()
	layer3_1.PrintResult()
	log.Printf("totalRequest %d used: %v", totalRequest, used)

}